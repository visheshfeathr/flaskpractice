from flask import render_template, flash, redirect, url_for, jsonify, request, g
from flask_login import current_user, login_user, logout_user, login_required
from app import app, lm
from auth import *
from .models import User, Post
from .forms import LoginForm, EditForm, PostForm
from datetime import datetime

# index view function suppressed for brevity

@app.before_request
def before_request():
	g.user = current_user

@lm.user_loader
def load_user(nickname):
	print 'User nickname is %s' %(User.objects(nickname=nickname).first().nickname)
	return User.objects(nickname=nickname).first()

@app.route('/login', methods=['GET', 'POST'])
def login():
	if g.user is not None and g.user.is_authenticated:
		return redirect(url_for('index'))
	form = LoginForm()
	if form.validate_on_submit():
		user = User.objects(nickname=form.username.data).first()
		if user:
			if user.password == form.password.data:
					login_user(user)
					return redirect(url_for('index'))
			else:
				flash('Invalid Credentials. Please try again')
		else:
			flash('No user found')
	return render_template('login.html', title='Sign In', form=form)

@app.route('/', methods=['GET', 'POST'])
@app.route('/index', methods=['GET', 'POST'])
@login_required
def index():
	form = PostForm()
	if form.validate_on_submit():
		post = Post(body=form.post_body.data, created_at=datetime.utcnow(), author=g.user.id)
		post.save()
		flash('You have created a new post')
		return redirect(url_for('index'))
	posts = g.user.followed_posts()
	print posts
	return render_template('index.html', title='Welcome', form=form, posts=posts)

@app.route('/user/<nickname_user>')
@login_required
def user(nickname_user):
	user = User.objects(nickname=nickname_user).first()
	print 'Profile User nickname is %s' %(user.nickname)
	if user == None:
		flash('User %s not found' %nickname_user)
		return redirect(url_for('index'))
	'''posts = [
		{
			'author' : {'nickname': 'Vish'},
			'body' : 'My First Post'
			},
		{
			'author' : {'nickname': 'Me'},
			'body' : 'My First Post Also'
			}
		]'''
	posts = Post.objects(author=user)
	return render_template('user.html', user=user, posts=posts)

@app.route('/edit', methods=['GET', 'POST'])
@login_required
def edit():
    form = EditForm()
    if form.validate_on_submit():
        User.objects(nickname=g.user.nickname).update(about_me=form.about_me.data)
        User.objects(nickname=g.user.nickname).update(email=form.email.data)
        flash('Your changes have been saved.')
        return redirect(url_for('edit'))
    else:
        form.email.data = g.user.email
        form.about_me.data = g.user.about_me
    return render_template('edit.html', form=form)

@app.route('/follow/<nickname>')
@login_required
def follow(nickname):
    user = User.objects(nickname=nickname).first()
    if user is None:
        flash('User %s not found.' % nickname)
        return redirect(url_for('index'))
    if user == g.user:
        flash('You can\'t follow yourself!')
        return redirect(url_for('user', nickname=nickname))
    u = g.user.follow(user)
    if u is None:
        flash('Cannot follow ' + nickname + '.')
        return redirect(url_for('user', nickname=nickname))
    User.objects(nickname=g.user.nickname).update(following=u.following)
    flash('You are now following ' + nickname + '!')
    return redirect(url_for('user', nickname_user=nickname))

@app.route('/unfollow/<nickname>')
@login_required
def unfollow(nickname):
    user = User.objects(nickname=nickname).first()
    if user is None:
        flash('User %s not found.' % nickname)
        return redirect(url_for('index'))
    if user == g.user:
        flash('You can\'t unfollow yourself!')
        return redirect(url_for('user', nickname=nickname))
    u = g.user.unfollow(user)
    if u is None:
        flash('Cannot unfollow ' + nickname + '.')
        return redirect(url_for('user', nickname=nickname))
    User.objects(nickname=g.user.nickname).update(following=u.following)
    flash('You have stopped following ' + nickname + '.')
    return redirect(url_for('user', nickname_user=nickname))

@app.route('/logout')
@login_required
def logout():
	logout_user()
	return redirect(url_for('login'))

@app.errorhandler(404)
def not_found_error(error):
	return render_template('404.html'), 404

@app.errorhandler(500)
def internal_error(error):
    return render_template('500.html'), 500