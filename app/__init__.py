from flask import Flask
from flask_mongoengine import MongoEngine
from flask_login import LoginManager, UserMixin
from config import basedir, ADMINS, MAIL_SERVER, MAIL_PORT, MAIL_USERNAME, MAIL_PASSWORD

app = Flask(__name__)
app.config["MONGODB_SETTINGS"] = {'DB': "microblog"}
app.config["SECRET_KEY"] = "KeepThisS3cr3t"
app.config.from_object('config')


db = MongoEngine(app)
lm = LoginManager(app)

if not app.debug:
	import logging
	from logging.handlers import SMTPHandler
	credentials = None
	if MAIL_USERNAME or MAIL_PASSWORD:
		credentials = (MAIL_USERNAME, MAIL_PASSWORD)
	mail_handler = SMTPHandler((MAIL_SERVER, MAIL_PORT), 'no-reply@' + MAIL_SERVER, ADMINS, 'microblog failure', credentials)
	mail_handler.setLevel(logging.ERROR)
	app.logger.addHandler(mail_handler)

from app import views, models