import datetime
from hashlib import md5
from app import db

class Post(db.Document):
    body = db.StringField(required=True)
    created_at = db.DateTimeField(default=datetime.datetime.now)
    author = db.ReferenceField('User')

    def __repr__(self):
        return '<Post %r>' % (self.body)

class User(db.Document):
    nickname = db.StringField(max_length=10, required=True)
    password = db.StringField(max_length=10, required=True)
    email = db.StringField(required=True)
    about_me = db.StringField(max_length=60)
    following = db.ListField(db.ReferenceField('User'))

    # def __init__(self, nickname, password, email):
    #     self.nickname = nickname
    #     self.password = password
    #     self.email = password

    # def __repr__(self):
    #     return 'User is %s with email %s' %(self.nickname, self.email)

    def follow(self, user):
        if not self.is_following(user):
            self.following.append(user)
        return self

    def unfollow(self, user):
        if self.is_following(user):
            self.following.remove(user)
        return self

    def is_following(self, user):
        if user in self.following:
            return True
        return False

    def followed_posts(self):
        followed_posts_temp = []
        for user in self.following:
            followed_posts_temp.extend(Post.objects(author=user.id))
        return followed_posts_temp

    def avatar(self, size):
        return 'http://www.gravatar.com/avatar/%s?d=mm&s=%d' % (md5(self.email.encode('utf-8')).hexdigest(), size)

    def is_authenticated(self):
        return True

    def is_anonymous(self):
        return False

    def is_active(self):
        return True

    def get_id(self):
        return self.nickname