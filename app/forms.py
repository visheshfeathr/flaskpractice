from flask_wtf import Form
from wtforms import StringField, BooleanField, PasswordField, TextAreaField
from wtforms.validators import DataRequired, Length

class LoginForm(Form):
	username = StringField('username', validators=[DataRequired()])
	password = PasswordField('password', validators=[DataRequired()])
	remember_me = BooleanField('remember_me', default=False)

class EditForm(Form):
    email = StringField('email', validators=[DataRequired()])
    about_me = StringField('about_me')

class PostForm(Form):
    post_body = StringField('post_body', validators=[DataRequired()])